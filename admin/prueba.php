<?php require "seguridad.php"; require "vistas.php" ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Documento</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <link rel="stylesheet" href="../css/estilos3.css">
    <script src="../js/jquery-3.2.1.min.js"></script>


  </head>
  <body>
    <header>
      <div class="container" id ="hd">
            <div class="col-md-6">
              <h1>GJ Backpacks : Anuncio</h1>

            <div class="col-md-6 text-right">
              <div class="dropdown" id="bot">
                <button class="btn btn-default dropdown-toggle" id="cuenta" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <?php echo $_SESSION["nombreUsuario"];?>
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                  <li><a href="salir.php">cerrar sesion</a></li>
                </ul>
              </div>
            </div>
            </div>
        </div>
      </div>
    </header>
    <section class= "admin" id="fondo">
      <div class="container">
        <div class="col-md-10 col-md-offset-1">
        <div class="row">

          <?php
            // si  tiene rol 1 osea admin muestro el boton para publicar...
           if($_SESSION["rol_id"]==1){ ?>
            <button type="btnNuevoAviso" class= "btn btn-primary" data-toggle="modal" data-target="#nuevoAviso">Publicar</button>
            <button type="btnListarVentas" class= "btn btn-primary" data-toggle="modal" data-target="#listarVentas">Listar Ventas</button>

            <button type="btnListarUsuarios" class= "btn btn-primary" data-toggle="modal" data-target="#listarUsuarios">Listar Usuarios</button>
          <?php   }     ?>
          <button type="btnNuevaVenta" class= "btn btn-primary" data-toggle="modal" data-target="#nuevaVenta">Venta</button>
          </div>
        <div class="row">
          <div class="">
            <aside id="columna">
          		<blockquote>Paso 1 : Ingresa la marca que deseas vender</blockquote>
          		<blockquote>Paso 2 : Escoge la cateogria adecuada para tu venta</blockquote>
          		<blockquote>Paso 3 : Seleciona una foto de tu producto</blockquote>
          		<blockquote>Paso 4 : Escriba el precio de venta</blockquote>
          	</aside>
          </div>
          <figure>
      		<img class="imgAviso" src="../img/aviso2.jpg">
      	</figure>
        </div>
      </div>
      </div>
    </section>

        <!-- listar usuarios -->
    <div class="modal fade" tabindex="-1" role="dialog" id="listarUsuarios">
      <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal1">
               <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id ="msnListarUsuarios">Listar Usuarios</h4>
               </div>
                <div class="modal-body">

                    <div class="listado_usuario">

                        <?php listado_usuarios(); ?>
                    </div>
                    <script>
                    $( document ).ready(function() {
                               $( ".delete_user" ).click(function() {
                                    id = $(this).attr( "id-usuario" );

                               tipo_operacion = 'eliminar_usuario';
                              // id = '100';

                              $.post("controlador.php", { tipoOperacion: tipo_operacion,usuario_id:id }, function(data){
                                alert(data);
                                location.reload();
                               });
                                   //$("#total").val($("#precio").val() * $("#cantidad").val());
                            });
                    });
                      </script>
                </div>

        </div>
      </div>
    </div>
<!-- -->

        <!-- listar ventas -->
    <div class="modal fade" tabindex="-1" role="dialog" id="listarVentas">
      <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal2">
               <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id ="msnListarVentas">Listar Ventas</h4>
               </div>
                <div class="modal-body">

                    <div class="listado_venta">

                        <?php listado_ventas(); ?>
                    </div>
              </div>

        </div>
      </div>
    </div>
<!-- -->



    <!-- nuevo aviso -->
    <div class="modal fade" tabindex="-1" role="dialog" id="nuevoAviso">
      <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal3">
               <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id ="msnNuevoAviso">Publicar</h4>
               </div>
                <div class="modal-body">
                          <form class="form-horizontal" id="formu-nuevoAviso">
                            <div class="form-group">
                              <label  class="col-sm-2 control-label">Titulo</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="Escriba la marca" name="titulo">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Categoria</label>
                              <div class="col-sm-10">
                                  <?php crearCategorias(); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Imagen</label>
                              <div class="col-sm-10">
                                <input type="file" class="form-control" id="inputPassword3" name="imagen">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Stock</label>
                              <div class="col-sm-10">
                                <input name="stock" class="form-control" type="text" ></input>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Precio</label>
                              <div class="col-sm-10">
                                <input name="precio" class="form-control" type="text" ></input>
                              </div>
                            </div>

                              <input type="hidden" name="tipoOperacion" value="insertarAviso">
                              <input type="hidden" name="idUsuario" value="<?php echo $_SESSION["idUser"]?>">
                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                              </div>
                            </div>
                          </form>
                </div>

        </div>
      </div>
    </div>
<!-- -->
<!-- ventas -->
<style type="text/css">
span#calcular:hover {
  cursor: pointer;

}
span#calcular {
  color:#ffffff;
  background:green;
   padding:10px;
   -moz-border-radius:10px/30px/30px;
 	-webkit-border-radius:10px/10px/10px;
 	border-radius:  10px;

}
</style>
<div class="modal fade" tabindex="-1" role="dialog" id="nuevaVenta">
      <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal4">
               <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id ="msnNuevaVenta">Vender</h4>
               </div>
                <div class="modal-body">
                          <form class="form-horizontal" id="formu-nuevaVenta">
                             <div class="form-group">
                                <label class="col-sm-2 control-label">Producto</label>
                                <div class="col-sm-10">
                                    <?php crearAvisos(); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Precio</label>
                                <div class="col-sm-10">
                                    <input type="text" name="precio" id="precio" readonly="false">
                                </div>
                           </div>
                           <div class="form-group">
                                <label class="col-sm-2 control-label">Cantidad</label>
                                 <div class="col-sm-10">
                                      <input type="text" name="cantidad" id="cantidad">
                                      <span style="" id="calcular">Calcular</span>
                                </div>
                           </div>
                           <div class="form-group">
                                <label class="col-sm-2 control-label">Total</label>
                               <div class="col-sm-10">
                                    <input type="text" name="total"  id="total" readonly="false">
                               </div>
                           </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Cliente</label>
                              <div class="col-sm-10">
                                <input name="cliente" class="form-control" type="text" ></input>
                              </div>
                            </div>

                              <input type="hidden" name="tipoOperacion" value="nuevaVenta">
                              <input type="hidden" name="idUsuario" value="<?php echo $_SESSION["idUser"]?>">
                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                              </div>
                            </div>
                          </form>
                              <script>
                            $( document ).ready(function() {
                                //evento para cuando seleccione un  product4o
                                     $("#lista_producto").change(function () {

                                      $("#lista_producto option:selected").each(function () {
                                        //leo su atribito y lo pongo en el campo preco
                                        precio = $(this).attr( "data-precio" );
                                        $("#precio").val(precio);


                                      });



                                  });
                                    $( "#calcular" ).click(function() {

                                       precio_j = $("#precio").val();
                                       cantidad_j = $("#cantidad").val();

                                        //$.post("views/ventas/calculo_total.php", { precio: precio_j,cantidad:cantidad_j }, function(data){
                                        //  $("#calculo_total").html(data);
                                        //});
                                           $("#total").val($("#precio").val() * $("#cantidad").val());
                                    });


                                   });
                              </script>
                </div>

        </div>
      </div>
    </div>
<!-- **** -->
    <footer id="pie">
      Derechos Reservados &copy; 2018-2019
    </footer>

<script src="../js/bootstrap.js"></script>
    <script src="../js/acciones.js"></script>

  </body>
</html>

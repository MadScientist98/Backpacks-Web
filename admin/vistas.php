<?php
require_once "conexion.php";

/*Incuiremos las consultas no destructicas, es decir , SELECT
listan contenido no lo eliminan*/
function inicioSesion($user , $pass){
  $mysql = conexionMySql();
  $sql = "SELECT * FROM usuario WHERE correo = '$user' AND password = '$pass'";
  $resExiste = mysqli_query($mysql, $sql);
  $fila= mysqli_fetch_array($resExiste);// para nombre de clave y posicion
 // print_r($fila);
  if(mysqli_num_rows($resExiste) > 0){
    session_start();
    $_SESSION["iniciado"] = true;
    $_SESSION["idUser"] = $fila[0];
    $_SESSION["rol_id"] = $fila[6];
    $_SESSION["nombreUsuario"] = $fila[1]. " ".$fila[2];
    $respuesta = 1;

  //  print_r( $_SESSION["rol_id"] ); die; 
  }else{
    $respuesta = "<span class ='msnError'>Datos Incorrectos!!</span>";
  }
  return printf($respuesta);
}

function crearCategorias(){
    $mysql = conexionMySql();
    $sql = "SELECT * FROM categoria";
    $res = mysqli_query($mysql, $sql);
    $respuesta = '<select class="form-control" name="Categoria"  required>';
            $respuesta .= '<option value = "">Selecione Categoria</option>';
            while($fila = mysqli_fetch_array($res)){
              $respuesta.='<option value="'.$fila[0].'">'.$fila[1].'</option>';
            }
    $respuesta .= '</select>';
    return printf($respuesta);
}

function crearAvisos(){
    $mysql = conexionMySql();
    $sql = "SELECT * FROM avisos";
    $res = mysqli_query($mysql, $sql);
    $respuesta = '<select class="form-control" name="Aviso" id="lista_producto" required>';
            $respuesta .= '<option value = "">Selecione Producto</option>';
            while($fila = mysqli_fetch_array($res)){
              $respuesta.='<option data-precio="'.$fila[2].'" value="'.$fila[0].'">'.$fila[1].' - S/.'.$fila[2].'</option>';
            }
    $respuesta .= '</select>';
    return printf($respuesta);
}

function listado_usuarios(){
    $mysql = conexionMySql();
    $sql = "SELECT * FROM usuario";
    $res = mysqli_query($mysql, $sql);
    $respuesta='<table border="1">
                  <tr>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Telefono</th>
                    <th>Correo</th>
                    <th>Acciones</th>
                  </tr>';
    
            while($fila = mysqli_fetch_array($res)){
              $respuesta .= '<tr>';
              $respuesta.='<td>'.$fila[1].'</td>'.'<td>'.$fila[2].'</td>'.'<td>'.$fila[3].'</td>'.'<td>'.$fila[4].'</td>';
              $respuesta.='<td> <span id-usuario="'.$fila[0].'" class="delete_user btn btn-danger"  >Eliminar</span></td>';
              $respuesta .= '</tr>';
            }
    $respuesta .= '</table>';
    return printf($respuesta);
}

function listado_ventas(){
    $mysql = conexionMySql();
    $sql = "SELECT  ve.id ,pr.titulo as producto, ve.cantidad , ve.precio, ve.total ,ve.cliente as cliente, us.nombre as usuario                           
                                              FROM ventas AS ve
                                              INNER JOIN usuario us ON ve.usuario_id=us.idusuario
                                              INNER JOIN avisos pr ON ve.producto_id=pr.idaviso;";
    $res = mysqli_query($mysql, $sql);
    $respuesta='<table border="1">
                  <tr>
                    <th>id</th>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th>Total</th>
                    <th>Cliente</th>
                    <th>Vendedor</th>
                  </tr>';
    
            while($fila = mysqli_fetch_array($res)){
              $respuesta .= '<tr>';
              $respuesta.='<td>'.$fila[0].'</td>'.'<td>'.$fila[1].'</td>'.'<td>'.$fila[2].'</td>'.'<td>'.$fila[3].'</td>'.'<td>'.$fila[4].'</td>'.'<td>'.$fila[5].'</td>'.'<td>'.$fila[6].'</td>';
              $respuesta .= '</tr>';
            }
    $respuesta .= '</table>';
    return printf($respuesta);
}

 ?>

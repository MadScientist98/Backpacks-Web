/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.25 : Database - bd-mochilas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bd-mochilas` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bd-mochilas`;

/*Table structure for table `avisos` */

DROP TABLE IF EXISTS `avisos`;

CREATE TABLE `avisos` (
  `idaviso` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `precio` float NOT NULL,
  `rutaimagen` varchar(255) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `idusuario` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  PRIMARY KEY (`idaviso`),
  KEY `fk_aviso_usuario_idx` (`idusuario`),
  KEY `fk_aviso_categoria1_idx` (`idcategoria`),
  CONSTRAINT `fk_aviso_categoria1` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_aviso_usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `avisos` */

insert  into `avisos`(`idaviso`,`titulo`,`precio`,`rutaimagen`,`stock`,`idusuario`,`idcategoria`) values (14,'Adidas',2,'img/02b986a80cfa06573918eccb3d616c67.jpeg',200,7,2),(18,'marcape',15,'img/2a11216e39d7b49b17c99b633da2b104.jpeg',13,15,2),(19,'eeee',11,'img/65469c653f5c25043ce014ed7ce4709f.jpeg',33,15,3),(20,'qqqq',32,'img/8f938372cfa96748785d37b5321a1689.jpeg',22,6,2),(21,'qqqq',12,'img/b3a3f3f44606419703c54c45fd66aacb.jpeg',232,15,2),(22,'45',12,'img/333499a3389823fc264b408e57c0cad6.jpeg',23,6,2),(23,'marcapesss',34,'img/08a237221e9a596359e3645ae0785f1b.jpeg',24,15,4);

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombreCategoria` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `categoria` */

insert  into `categoria`(`idcategoria`,`nombreCategoria`) values (1,'Escolar'),(2,'Deportiva'),(3,'Tecnologica'),(4,'Casual');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`descripcion`) values (1,'admin'),(2,'vendedor');

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `password` varchar(32) NOT NULL,
  `rol_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `usuario` */

insert  into `usuario`(`idusuario`,`nombre`,`apellido`,`telefono`,`correo`,`password`,`rol_id`) values (6,'Elon','Musk','983444211','vendor@hotmail.com','123',2),(7,'Gonzalo','Caira','91327103','gcaira@gmail.com','123',2),(8,'Joao','Chavez','980754999','joaochavezsalas@gmail.com','123',2),(10,'Fabricio','Barrionuevo','974624512','fbarrionuevo@gmail.com','123',2),(12,'Mario','Pierola','97412311','mp@gmail.com','123',2),(14,'Lucas','Beras','972313546','lb@gmail.com','123',2),(15,'juni','gomez','3333','loqui@hotmail.com','123',1);

/*Table structure for table `ventas` */

DROP TABLE IF EXISTS `ventas`;

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` decimal(10,0) DEFAULT NULL,
  `cliente` varchar(100) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `precio` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `ventas` */

insert  into `ventas`(`id`,`total`,`cliente`,`usuario_id`,`cantidad`,`producto_id`,`precio`) values (1,150,'21',6,5,14,30),(2,50,'23',6,2,17,25),(3,250,'22',6,5,17,50),(4,250,'20',6,5,14,50),(5,60,'14',6,2,17,30),(6,280,'21',6,5,17,56),(7,224,'1',6,4,14,56),(8,100,'1',6,4,14,25),(9,175,'1',6,7,18,25),(10,750,'juloo',6,3,15,250),(11,300,'loquii',6,2,16,150),(12,55,'loquirufo',6,5,14,11),(13,102,'tony blaseeee',15,3,23,34),(14,136,'ositaa',15,4,23,34);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

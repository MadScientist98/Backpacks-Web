<?php
require_once "admin/modelo.php";

$avisos  = listar_avisos();


function  lista_categoria(){
    $mysql = conexionMySql();
    $sql = "SELECT * FROM categoria";
    $res = mysqli_query($mysql, $sql);
    $respuesta = '<select class="form-control" name="Categoria" id="lista_cate" required>';
            $respuesta .= '<option value = "">Selecione Categoria</option>';
            while($fila = mysqli_fetch_array($res)){
              $respuesta.='<option value="'.$fila[0].'">'.$fila[1].'</option>';
            }
    $respuesta .= '</select>';
    return printf($respuesta);
}


?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mochilas</title>
    <meta name="description" content="Sitio web para compra de mochilas">
    <meta name="keywords" content="mochilas , venta , avisos , precios menor,precio por marcas">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fuentes.css">
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/estiloH.css">
      <script src="js/jquery-3.2.1.min.js"></script>
  </head>
  <body>
    <header>
      <div class="container">
        <div class="col-md-6">
          <h1>GJ Backpacks</h1>
        </div>
        <div class="col-md-6">
        <div class="botonesCabecera">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inicioSesion">Iniciar Sesion</button>
          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#Registrarme">Registrarte Ahora!</button>
        </div>
      </div>
    </div>
  </header>
  <div class="">
  </div>
  <div class="">
    <figure>
		<img src="img/Portada2.jpg">
	</figure>

  </div>
  <nav id="menu">
			<ul style="display:inline-block; border-right:2px solid #000;">
				<li>Categoria : <?php lista_categoria(); ?> <button id="filtro_categoria" class="btn btn-primary">Buscar</button></li>

	</nav>
  <script>
  $( document ).ready(function() {
             $( "#filtro_categoria" ).click(function() {

                    //   $("#lista_cate option:selected").each(function () {
                        //leo su atribito y lo pongo en el campo preco
                       // precio = $(this).attr( "data" );
                        //$("#precio").val(precio);


                    //  });
               tipo_operacion = 'listar_avisos_cate';
               id = $("#lista_cate").val()

            $.post("filtro_categoria.php", { tipoOperacion: tipo_operacion,categoria_id:id }, function(data){

                 $("#listado").html(data);
          });
        });
   });
    </script>
  <div id="listado">
  <?php
//   con este while listamos los proucctos
       while ( $productos = mysqli_fetch_array($avisos)) {


     ?>
      <div class="seccion" id="cuadro">
        <article>
          <figure>
            <img class="centrado" src="admin/<?php echo $productos['rutaimagen']; ?>" style="width:200px; height:200px;">
            <figcaption>
              <p><strong>Titulo:  </strong><?php echo $productos['titulo']; ?></p>
              <p><strong>Precio:  </strong><?php echo $productos['precio']; ?></p>
              <p><strong>Unidades:  </strong><?php echo $productos['stock']; ?></p>
            </figcaption>
          </figure>
        </article>
      </div>
    <?php } ?>
  </div>
<!--
	<aside id="columna">
		<blockquote>POR MARCA</blockquote>
		<blockquote>POR COLOR</blockquote>
		<blockquote>POR TIPO</blockquote>
		<blockquote>POR PRECIOS</blockquote>
	</aside>
-->

  <footer id="pie">
    Derechos Reservados &copy; 2018-2019
  </footer>

    <?php require "ventanasModales.html";  ?>

    <script src="js/bootstrap.js"></script>
    <script src="js/acciones.js"></script>

    </script>
  </body>
</html>
